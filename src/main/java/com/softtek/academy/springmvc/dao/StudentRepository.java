package com.softtek.academy.springmvc.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.softtek.academy.springmvc.model.Student;

@Repository("studentRepository")
public class StudentRepository implements StudentDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void saveStudent(Student s) {
		 entityManager.persist(s);
	}

	@Override
	public void updateStudent(Student s) {
		entityManager.persist(s);
	}

	@Override
	public void deleteStudent(Student s) {
		entityManager.remove(s);
	}

	@Override
	public void deleteById(int Id) {
		Student student = entityManager.find(Student.class, Id);
        if (student != null) {
        	entityManager.getTransaction().begin();
        	entityManager.remove(student);
        	entityManager.getTransaction().commit();
        }
		
	}

	@Override
	public Student findById(int Id) {
		return entityManager.find(Student.class, Id);
	}

	@Override
	public List<Student> getAllStudents() {
		return (List<Student>)entityManager
				.createQuery("Select c from Student c", Student.class).getResultList();
	}

}
