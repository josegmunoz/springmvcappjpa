package com.softtek.academy.springmvc.dao;

import java.util.List;

import com.softtek.academy.springmvc.model.Student;

public interface StudentDao {
	void saveStudent(Student s);
	
	void updateStudent(Student s);
	
	void deleteStudent(Student s);
	
	void deleteById(int Id);
	
	Student findById(int Id);

	List<Student> getAllStudents();
}
