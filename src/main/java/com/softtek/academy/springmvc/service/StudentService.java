package com.softtek.academy.springmvc.service;

import java.util.List;

import com.softtek.academy.springmvc.model.Student;

public interface StudentService {
	
	void updateStudent(Student s);
	
	void deleteStudent(Student s);
	
	void saveStudent(Student s);

	void deleteById(int Id);

	List<Student> getAllStudents();

	Student findById(int Id);
}

/*
public interface ColorService {

List<Color> getAll();

Color getById(long id);

List<Color> getFilterList();
}
*/