package com.softtek.academy.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.softtek.academy.springmvc.dao.StudentDao;
import com.softtek.academy.springmvc.model.Student;

@Service
public class StudentServiceImpl implements StudentService {
	
	@Autowired
	@Qualifier("studentRepository")
	private StudentDao studentDao;
	
	
	
	public StudentServiceImpl(StudentDao studentDao) {
		this.studentDao = studentDao;
	}

	@Override
	public void updateStudent(Student s) {
		this.studentDao.updateStudent(s);

	}

	@Override
	@Transactional
	public void deleteStudent(Student s) {
		this.studentDao.deleteStudent(s);
	}

	@Override
	public void saveStudent(Student s) {
		this.studentDao.saveStudent(s);
	}
	@Override
	public void deleteById(int Id) {
		studentDao.deleteById(Id);
	}

	@Override
	public Student findById(int Id) {
		return studentDao.findById(Id);
	}

	@Override
	public List<Student> getAllStudents() {
		return studentDao.getAllStudents();
	}


}
