package com.softtek.academy.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.academy.springmvc.model.Person;

@Controller
public class PersonController {
	
	@RequestMapping(value ="/person", method = RequestMethod.GET)
	public ModelAndView Person(){
		return new ModelAndView("person","command", new Person());
	}
	
	@RequestMapping(value="/addPerson", method = RequestMethod.GET)
	public ModelAndView addPerson() {
		return new ModelAndView("person","command", new Person());
	}
	
}
