package com.softtek.academy.springmvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.academy.springmvc.model.Student;
import com.softtek.academy.springmvc.service.StudentService;

@Controller
public class StudentController {

	@Autowired
	private StudentService studentService;
	
	@RequestMapping(value ="/students", method = RequestMethod.GET)
	public ModelAndView student() {
		List<Student> students = studentService.getAllStudents();
		ModelAndView page = new ModelAndView("students");
		page.addObject("students", students);
		return page;
	}
	
	
	@RequestMapping(value = "student/{id}", method = RequestMethod.GET)
	public Student student(@PathVariable int id) {
		return studentService.findById(id);
	}
	
	
	
	
	
}
