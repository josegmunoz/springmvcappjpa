package com.softtek.academia.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import com.softtek.academy.springmvc.configuration.JpaConfiguration;
import com.softtek.academy.springmvc.model.Student;
import com.softtek.academy.springmvc.service.StudentService;

@ContextConfiguration(classes= {JpaConfiguration.class})
@WebAppConfiguration
public class StudentServiceTest {
	@Autowired
    private StudentService studentService;

    @Test
    public void testGetById(){
        // setup
        int id = 3;
        Student expectedStudent = new  Student(3, 23, "Panchito");
        //execute
        Student actualStudent =  studentService.findById(id);
        //validate
        assertNotNull(actualStudent);
        //assertEquals(expectedStudent.getName(), actualStudent.getName());
    }

}
